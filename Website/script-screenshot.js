//fungsi buka tutup sidenav
function openSidenav() {
    document.getElementById("sidenav-1").style.width = "250px";
}

function closeSidenav() {
    document.getElementById("sidenav-1").style.width = "0px";
}

//fungsi untuk menganimasikan screenshot gameplay ketika discroll
window.addEventListener('scroll', function() {
    let header = document.getElementsByTagName("header")[0];
    let gallery = document.getElementsByClassName("gallery")[0];
    var scrollValue = window.scrollY;

    if (scrollValue >= (header.offsetHeight * 0.8) - 200) {
        gallery.classList.add("animated");
    }
    else {
        gallery.classList.remove("animated");
    }
})