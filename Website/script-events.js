//fungsi buka tutup sidenav
function openSidenav() {
    document.getElementById("sidenav-1").style.width = "250px";
}

function closeSidenav() {
    document.getElementById("sidenav-1").style.width = "0px";
}

//fungsi untuk menganimasikan "loading" post event ketika discroll
window.addEventListener("scroll", function() {
    let header = document.getElementsByTagName("header")[0];
    let event1 = document.getElementsByClassName("event-1")[0];
    let event2 = document.getElementsByClassName("event-2")[0];
    let event3 = document.getElementsByClassName("event-3")[0];

    var scrollValue = window.scrollY;
    if (scrollValue >= (header.offsetHeight * 0.8) - 200) {
        event1.classList.add("animated-rtl");
    }
    else {
        event1.classList.remove("animated-rtl");
    }
    if (scrollValue >= ((header.offsetHeight + event1.offsetHeight)* 0.8) - 200) {
        event2.classList.add("animated-ltr");
    }
    else {
        event2.classList.remove("animated-ltr");
    }
    if (scrollValue >= ((header.offsetHeight + event1.offsetHeight + event2.offsetHeight)* 0.8) - 200) {
        event3.classList.add("animated-rtl");
    }
    else {
        event3.classList.remove("animated-rtl");
    }
})