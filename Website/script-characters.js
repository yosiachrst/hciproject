//fungsi buka tutup sidenav
function openSidenav() {
    document.getElementById("sidenav-1").style.width = "250px";
}

function closeSidenav() {
    document.getElementById("sidenav-1").style.width = "0px";
}

//untuk mengganti karakter jika pengguna mengklik salah satu character.
function displayCharacters(newSource, newTitle, newNickname, newDescription) {
    let cards = document.getElementsByClassName("character-cards")[0];
    let imageContainer = document.getElementsByClassName("image")[0];
    let title = document.getElementsByClassName("title")[0];
    let nick = document.getElementsByClassName("nickname")[0];
    let description = document.getElementsByClassName("description")[0];


    cards.classList.remove("animated");
    cards.classList.add("default");
    
    setTimeout(function() {
        imageContainer.src = newSource;
        title.innerHTML = newTitle;
        nick.innerHTML = newNickname;
        description.innerHTML = newDescription;
    
        cards.classList.add("animated");
    }, 300);


}