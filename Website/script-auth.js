/* untuk buka tutup sidenav */

function openSidenav() {
    document.getElementById("sidenav-1").style.width = "250px";
}

function closeSidenav() {
    document.getElementById("sidenav-1").style.width = "0px";
}

//flag untuk menyatakan bahwa validasi sudah tepat atau belum. jika false, berarti masih belum valid.
var checkEmail = false;
var checkName = false;
var checkGender = false;
var checkUsername = false;
var checkPassword = false;
var checkEula = false;

//pengambilan input pengguna sebagai variabel js
var submitBtn = document.getElementById("submitBtn");
var nameErrMsg = document.getElementById("err-name");
var emailErrMsg = document.getElementById("err-email");
var usernameErrMsg = document.getElementById("err-username");
var passwordErrMsg = document.getElementById("err-password");
function validateForm() {
    //jika 1 field sudah valid, maka validCount akan ditambah 1, dan seterusnya.
    //(flag untuk menyatakan berapa field yang sudah valid atau sesuai format)
    var validCount = 0;
    if (checkEmail) {
        emailErrMsg.innerHTML = "";
        validCount++;
    }
    if (checkName) {
        nameErrMsg.innerHTML = "";
        validCount++;
    }
    if (checkGender) {
        validCount++;
    }
    if (checkUsername) {
        usernameErrMsg.innerHTML = "";
        validCount++;
    }
    if (checkPassword) {
        passwordErrMsg.innerHTML = "";
        validCount++;
    }
    if (checkEula) {
        validCount++;
    }
    //jika semua field yang dimiliki valid, maka tombol submit dapat diklik
    if (validCount == 6) {
        submitBtn.disabled = false;
    }
    //jika belum valid, maka tombol submit belum bisa diklik
    else {
        submitBtn.disabled = true;
    }
}

//fungsi untuk validasi nama
function validateName(name) {
    //jika nama kosong
    if (name == "") {
        checkName = false;
        //beri error message, nama harus diisi
        nameErrMsg.innerHTML = "Name must be filled!";
        return;
    }
    else {
        for (let i = 0; i < name.length; i++) {
            //pengecekan jika ada integer atau special characters
            if (name.charCodeAt(i) < 65 || (name.charCodeAt(i) > 90 && name.charCodeAt(i) < 97) || name.charCodeAt(i) > 122) {
                checkName = false;
                //beri error message, nama tidak dapat memuat integer / special characters
                nameErrMsg.innerHTML = "Name cannot contains numbers and special characters!";
                return;
            }
        }
    }
    //nama sudah valid
    checkName = true;
    return;
}


//fungsi pengecekan email
function validateEmail(email) {
    //jika kosong
    if (email == "") {
        checkEmail = false;
        emailErrMsg.innerHTML = "Email must be filled!";
        return;
    }
    //cari first index untuk '@'
    var firstAtIndex = email.indexOf("@");
    //cari last index untuk '@
    var lastAtIndex = email.lastIndexOf("@");
    //jika first index dan last index untuk '@' tidak sama, maka dipastikan ada lebih dari 1 '@'
    if (firstAtIndex != lastAtIndex) {
        checkEmail = false;
        //maka beri error, email tidak dapat memuat lebih dari 1 '@'
        emailErrMsg.innerHTML = "Email cannot contains two or more @!";
        return;
    }
    //jika tidak ada '@', format email invalid.
    if (firstAtIndex == -1) {
        checkEmail = false;
        emailErrMsg.innerHTML = "Email format is invalid!";
    }
    var splittedEmail = email.split("@");
    //split email, string sebelum '@' dan setelah '@'
    var emailBeforeAt = splittedEmail[0];
    var emailAfterAt = splittedEmail[1];
    //cari simbol titik yang pertama dan terakhir.
    var firstPointIndex_f = emailBeforeAt.indexOf(".");
    var lastPointIndex_f = emailBeforeAt.lastIndexOf(".");
    var iteratingIndex_f = firstPointIndex_f;
    if (firstPointIndex_f != -1) {
        //jika ada titik yang berurutan, format email invalid.
        for (let i = 0; i < emailBeforeAt.length; i++) {
            if (emailBeforeAt[i] == '.' && i > iteratingIndex_f) {
                if (i - iteratingIndex_f == 1) {
                    checkEmail = false;
                    emailErrMsg.innerHTML = "Email format is invalid!";
                    return;
                }
                else {
                    iteratingIndex_f = i;
                }
            }
        }
        //jika titik ditemukan < 2 karakter sebelum @, maka email invalid,
        //contoh: asdsad.@com, asdsad.a@com.
        if (lastPointIndex_f + 2 > emailBeforeAt.length - 1) {
            checkEmail = false;
            emailErrMsg.innerHTML = "Email format is invalid!";
            return;
        }
        //jika titik ada di index pertama, email juga invalid
        //contoh: .asdsad@com
        if (firstPointIndex_f == 0) {
            checkEmail = false;
            emailErrMsg.innerHTML = "Email format is invalid!";
            return;
        }
    }

    var firstPointIndex_r = emailAfterAt.indexOf(".");
    var lastPointIndex_r = emailAfterAt.lastIndexOf(".");
    var iteratingIndex_r = firstPointIndex_r;
    //cari titik yang berurutan, kalau ada, invalid
    if (firstPointIndex_r != -1) {
        for (let i = 0; i < emailAfterAt.length; i++) {
            if (emailAfterAt[i] == '.' && i > iteratingIndex_r) {
                if (i - iteratingIndex_r == 1) {
                    checkEmail = false;
                    emailErrMsg.innerHTML = "Email format is invalid!";
                    return;
                }
                else {
                    iteratingIndex_r = i;
                }
            }
        }
        if (firstPointIndex_r - 2 < 0) {
            checkEmail = false;
            emailErrMsg.innerHTML = "Email format is invalid!";
            return;
        }
        //titik terakhir tidak boleh ada di akhir string
        if (lastPointIndex_r == emailAfterAt.length - 1) {
            checkEmail = false;
            emailErrMsg.innerHTML = "Email format is invalid!";
            return;
        }
        //kalau sudah melewati semuanya, email valid.
        checkEmail = true;
        return;
    }
    else {
        //kalau ga ada titik, email invalid juga karena string ini dimuat setelah simbol '@'
        checkEmail = false;
        emailErrMsg.innerHTML = "Email format is invalid!";
        return;
    }
}

function validateGender(gender) {
    if (gender == "") {
        checkGender = false;
        return;
    }
    checkGender = true;
    return;
}

function validateUsername(username) {
    if (username == "") {
        checkUsername = false;
        usernameErrMsg.innerHTML = "Username must be filled!";
        return;
    }
    else {
        if (username.length <= 5) {
            checkUsername = false;
            usernameErrMsg.innerHTML = "Username length must be longer than 5 characters!";
            return;
        }
        checkUsername = true;
        return;
    }
}

function validatePassword(password) {
    if (password == "") {
        checkPassword = false;
        passwordErrMsg.innerHTML = "Password must be filled!";
        return;
    }
    else {
        if (password.length < 8) {
            checkPassword = false;
            passwordErrMsg.innerHTML = "Password length must be longer than 8 characters!";
            return;
        }
        checkPassword = true;
        return;
    }
}

function validateEula(eula) {
    if (eula.checked) {
        checkEula = true;
        return;
    }
    else {
        checkEula = false;
        return;
    }
}

window.addEventListener('keyup', validateForm);
window.addEventListener('change', validateForm);