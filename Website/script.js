//fungsi buka tutup sidenav
function openSidenav() {
    document.getElementById("sidenav-1").style.width = "250px";
}

function closeSidenav() {
    document.getElementById("sidenav-1").style.width = "0px";
}

//fungsi untuk menganimasikan setiap elemen home ketika discroll,
window.addEventListener("scroll", function() {
    let title = document.getElementsByClassName("title")[0];
    let header = document.getElementsByClassName("img-container")[0];
    let paragraph = document.getElementsByTagName("p");
    let features = document.getElementsByClassName("features")[0];

    var scrollValue = window.scrollY;
    if (scrollValue >= (header.offsetHeight * 0.8) - 200) {
        title.classList.add("animated");
        features.classList.add("animated");
        for (let i = 0; i < paragraph.length; i++) {
            paragraph[i].classList.add("animated");
        }
    }
    else {
        title.classList.remove("animated");
        features.classList.remove("animated");
        for (let i = 0; i < paragraph.length; i++) {
            paragraph[i].classList.remove("animated");
        }
    }
})

window.addEventListener("scroll", function() {
    let header = document.getElementsByClassName("img-container")[0];
    let about = document.getElementsByClassName("about")[0];
    let title = document.getElementsByClassName("title")[1];
    let subtitle = document.getElementsByClassName("subtitle");
    let hero = document.getElementsByTagName("img");

    var scrollValue = window.scrollY;
    if (scrollValue >= ((header.offsetHeight + about.offsetHeight)* 0.8) - 200) {
        title.classList.add("animated");
        for (let i = 0; i < hero.length; i++) {
            hero[i].classList.add("animated");
        }
        for (let i = 0; i < subtitle.length; i++) {
            subtitle[i].classList.add("animated");
        }
    }
    else {
        title.classList.remove("animated");
        for (let i = 0; i < hero.length; i++) {
            hero[i].classList.remove("animated");
        }
        for (let i = 0; i < subtitle.length; i++) {
            subtitle[i].classList.remove("animated");
        }
    }
})

$(document).ready(function(){
    var x = 0;
    var totalImage = 200;
    var autoSlide;
    //fungsi untuk memulai image slider yang otomatis
    function startSlider() {
      autoSlide = setInterval(nextSlide, 3000);
    }
    //fungsi untuk menghentikan image slider yang otomatis
    function pauseSlider() {
      clearInterval(autoSlide);
    }
    //fungsi untuk menggeser gambar pada image slider secara manual (ke gambar selanjutnya)
    //jika tombol diklik hingga lebih dari jumlah gambar, maka akan kembali ke gambar paling awal.
    function nextSlide() {
      x= (x<200)?(x+100):0;
      //$('figure').css('left', -x+'%');
      $('figure').animate({left: -x + '%'});
    }
    //ketika tombol "next" tidak dihover, maka setInterval akan dihapus, kemudian dimulai kembali 
    //agar transisi tidak terlalu cepat
    $('.next').on('mouseleave', function() {
      pauseSlider();
      startSlider();
    });
    //memasukkan fungsi nextSlide() dan pauseSlider() ketika tombol "next" diklik
    //hal ini dimaksudkan supaya ketika pengunjung mengklik tombol next, maka image slider akan berhenti (dipause)
    //hingga pengunjung tidak lagi menghover tombol "next"
    $('.next').click(function() {
      nextSlide();
      pauseSlider();
    });
    //fungsi untuk menggeser gambar pada image slider secara manual (ke gambar sebelumnya)
    //jika tombol diklik hingga lebih dari jumlah gambar, maka akan kembali ke gambar paling awal.
    function previousSlide() {
      x= (x>=100)?(x-100):200;
      //$('figure').css('left', -x+'%');
      $('figure').animate({left: -x + '%'});
    }
    //memasukkan fungsi previousSlide() dan pauseSlider() ketika tombol "prev" diklik
    //hal ini dimaksudkan supaya ketika pengunjung mengklik tombol next, maka image slider akan berhenti (dipause)
    //hingga pengunjung tidak lagi menghover tombol "prev"
    $('.prev').click(function() {
      previousSlide();
      pauseSlider();
    });
    //ketika tombol "prev" tidak dihover, maka setInterval akan dihapus, kemudian dimulai kembali 
    //agar transisi tidak terlalu cepat
    $('.prev').on('mouseleave', function() {
      pauseSlider();
      startSlider();
    });
    //ketika image slider dihover, maka slider akan dipause hingga pengunjung tidak lagi menghover image slider
    //hal ini diperlukan agar memudahkan pengguna untuk melihat informasi yang diinginkan tanpa harus terganggu
    //image slider yang otomatis
    $('figure')
      .on('mouseenter', pauseSlider)
      .on('mouseleave', startSlider);
    
    //awalnya, image slider berjalan secara otomatis hingga tombol next / prev diklik.
    startSlider();
  });